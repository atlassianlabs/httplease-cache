# httplease-cache

This is an implementation of HTTP caching as an [httplease](https://bitbucket.org/atlassianlabs/httplease) filter.

Supports:

* Cache-Control directives: no-cache and max-age
* Expires header
* Age header
* Custom cache keys
* Custom caches, must conform to the node-cache interface
* Cache metrics (using node-cache)


# Usage guide

Install the library:

```
npm install --save httplease-cache
```

For more examples have a look at the `test/integration` directory.


## Simple configuration

```
const httplease = require('httplease');
const createCacheFilter = require('httplease-cache').createCacheFilter;

// this can be saved and reused as many times as you want
const httpClient = httplease.builder()
    .withBaseUrl('http://example.com/basePath')
    .withFilter(createCacheFilter());

// make requests
httpClient
    .withPath('/resource)
    .withMethodGet()
    .send();
```


## Configuration options

### generateCacheKey

You may specify a custom function for generating cache keys. This allows you to include or exclude parts of the request in the cache lookup key.

The default function includes the URL, request body, method, query params and headers.

```
const opts = {
    generateCacheKey: (requestConfig) => {
        return requestConfig.baseUrl;
    }
};

createCacheFilter(opts);
```

### theCache

You may specify a custom cache object. It should either be an instance of [node-cache](https://www.npmjs.com/package/node-cache) or implement the same async interface. That is:

```
const opts = {
    theCache: {
        get: function(cacheKey, callback) {
            callback(this[cacheKey]);
        },
        set: function(cacheKey, response, ttl, callback) {
            this[cacheKey] = response;
            // must pay attention to ttl as well!
            callback();
        }
    }
};

createCacheFilter(opts);
```


# Development guide

## Install dependencies

```
npm install
```


## Useful commands

```
# Run all checks
npm test

# Run just the jasmine tests
npm run test:jasmine

# Run just the linter
npm run test:lint
```


## Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# License

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
