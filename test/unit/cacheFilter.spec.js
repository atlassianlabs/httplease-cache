'use strict';

const createCacheFilter = require('../../lib/cacheFilter');

describe('createCacheFilter', () => {
    let fakeResponse;
    let fakeNext;
    let requestConfig;
    let theCache;
    let fakeCachedData;

    beforeEach(() => {
        fakeResponse = {
            body: 'body',
            headers: {
                'date': 'Fri, 01 Jan 2016 10:00:00 UTC',
                'cache-control': 'max-age=600'
            },
            status: 200
        };

        requestConfig = {
            httpOptions: {
                protocol: 'http',
                hostname: 'example.com',
                port: 1234,
                method: 'FAKEMETHOD',
                path: '/thepath?key=var',
                headers: {'Custom-Header': 'the value'},
                ignoredData: 'this is ignored'
            },
            ignoredData: 'this is also ignored'
        };

        fakeNext = jasmine.createSpy('next');
        fakeNext.and.returnValue(Promise.resolve(fakeResponse));

        theCache = jasmine.createSpyObj('theCache', ['set', 'get']);
        theCache.set.and.callFake((key, value, ttl, callback) => callback()); // eslint-disable-line max-params
        fakeCachedData = undefined;
        theCache.get.and.callFake((key, callback) => callback(null, fakeCachedData));

        jasmine.clock().install();
        jasmine.clock().mockDate(new Date('2016-01-01 10:00:00 UTC'));
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    function runFilter() {
        return createCacheFilter({theCache})(requestConfig, fakeNext);
    }

    function expectCacheStoreWithTTL(ttl) {
        expect(theCache.set).toHaveBeenCalledWith(
            jasmine.any(String),
            fakeResponse,
            ttl,
            jasmine.any(Function)
        );
    }


    describe('next(requestConfig)', () => {

        it('is called when storing into the cache', () => {
            return runFilter().then((response) => {
                expect(theCache.set).toHaveBeenCalled();
                expect(fakeNext).toHaveBeenCalledWith(requestConfig);
                expect(response).toBe(fakeResponse);
            });
        });

        it('is called when not storing into the cache', () => {
            fakeResponse.headers['cache-control'] = 'no-cache';

            return runFilter().then((response) => {
                expect(theCache.set).not.toHaveBeenCalled();
                expect(fakeNext).toHaveBeenCalledWith(requestConfig);
                expect(response).toBe(fakeResponse);
            });
        });

        it('is not called when returning data from the cache', () => {
            fakeCachedData = 'opaque fake cached response';

            return runFilter().then((response) => {
                expect(fakeNext).not.toHaveBeenCalledWith(requestConfig);
                expect(response).toBe('opaque fake cached response');
            });
        });

        it('is called using the default cache config', () => {
            return createCacheFilter()(requestConfig, fakeNext).then((response) => {
                expect(fakeNext).toHaveBeenCalledWith(requestConfig);
                expect(response).toBe(fakeResponse);
            });
        });

    }); // calling next

    describe('cache key generation', () => {

        it('by default looks up with a unique key built from the requestConfig', () => {
            const key = '{"protocol":"http","hostname":"example.com","port":1234,"method":"FAKEMETHOD","path":"/thepath?key=var","headers":{"Custom-Header":"the value"}}'; // eslint-disable-line max-len
            return runFilter().then(() => {
                expect(theCache.get).toHaveBeenCalledWith(
                    key,
                    jasmine.any(Function)
                );
                expect(theCache.set).toHaveBeenCalledWith(
                    key,
                    fakeResponse,
                    jasmine.any(Number),
                    jasmine.any(Function)
                );
            });
        });

        it('accepts a custom generateCacheKey function and uses it for lookups and stores', () => {
            function generateCacheKey() {
                return 'the magic key';
            }

            return createCacheFilter({theCache, generateCacheKey})(requestConfig, fakeNext).then(() => {
                expect(theCache.get).toHaveBeenCalledWith(
                    'the magic key',
                    jasmine.any(Function)
                );
                expect(theCache.set).toHaveBeenCalledWith(
                    'the magic key',
                    fakeResponse,
                    jasmine.any(Number),
                    jasmine.any(Function)
                );
            });
        });

    }); // end cache key generation

    describe('with cache-control', () => {

        beforeEach(() => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:00:00 UTC'));

            fakeResponse.headers = {
                'date': 'Fri, 01 Jan 2016 10:00:00 UTC',
                'cache-control': 'max-age=600'
            };
        });

        it('stores with ttl=max-age when the response is brand new', () => {
            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('stores with ttl=max-age when the response was created in the future', () => {
            jasmine.clock().mockDate(new Date('2016-01-01 09:00:00 UTC'));

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('stores with ttl=(max-age - 1min) when the response is 1min old', () => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:01:00 UTC'));

            return runFilter().then(() => {
                expectCacheStoreWithTTL(540);
            });
        });

        it('does not store when the response is too old', () => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:11:00 UTC'));

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when no-cache is set', () => {
            fakeResponse.headers['cache-control'] = 'max-age=600,no-cache';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when no-cache is set with some ignored value', () => {
            fakeResponse.headers['cache-control'] = 'max-age=600,no-cache=0';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when max-age=0', () => {
            fakeResponse.headers['cache-control'] = 'max-age=0';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('stores as if date=now when date is invalid', () => {
            fakeResponse.headers['date'] = 'not a date';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('does not store when max-age=-1', () => {
            fakeResponse.headers['cache-control'] = 'max-age=-1';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when max-age=foo', () => {
            fakeResponse.headers['cache-control'] = 'max-age=foo';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store with broken empty cache-control', () => {
            fakeResponse.headers['cache-control'] = ' ; ;  ';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('gracefully continues to store with broken cache-control containing max-age', () => {
            fakeResponse.headers['cache-control'] = 'WEIRD broken,  max-age=600  ,,, random stuff!!=@@symbols ';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('prefers cache-control to expires', () => {
            fakeResponse.headers['expires'] = 'Fri, 01 Jan 2017 10:10:00 UTC';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

    }); // end cache-control

    describe('with expires header', () => {

        beforeEach(() => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:00:00 UTC'));

            fakeResponse.headers = {
                'date': 'Fri, 01 Jan 2016 10:00:00 UTC',
                'expires': 'Fri, 01 Jan 2016 10:10:00 UTC'
            };
        });

        it('stores with ttl=(expires-date) when the response is brand new', () => {
            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('stores with ttl=(expires-date) when the response was created in the future', () => {
            // RFC 2616 Section 13.2.3
            //   freshness_lifetime = expires_value - date_value
            //   Note that neither of these calculations is vulnerable to clock
            //   skew, since all of the information comes from the origin server.

            jasmine.clock().mockDate(new Date('2016-01-01 09:00:00 UTC'));

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('stores with ttl=(max-age - 1min) when the response is 1min old', () => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:01:00 UTC'));

            return runFilter().then(() => {
                expectCacheStoreWithTTL(540);
            });
        });

        it('does not store when the response is too old', () => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:11:00 UTC'));

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when expires=0', () => {
            fakeResponse.headers['expires'] = '0';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when cache-control=no-cache', () => {
            fakeResponse.headers['cache-control'] = 'no-cache';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('does not store when expires is before date', () => {
            fakeResponse.headers['expires'] = 'Fri, 01 Jan 2016 09:00:00 UTC';

            return runFilter().then(() => {
                expect(theCache.set).not.toHaveBeenCalled();
            });
        });

        it('stores as if date=now when date is invalid', () => {
            fakeResponse.headers['date'] = 'not a date';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

    }); // end expires header

    describe('with age header', () => {

        beforeEach(() => {
            jasmine.clock().mockDate(new Date('2016-01-01 10:00:00 UTC'));

            fakeResponse.headers = {
                'date': 'Fri, 01 Jan 2016 10:00:00 UTC',
                'cache-control': 'max-age=600',
                'age': 100
            };
        });

        it('stores with ttl=(max-age - age)', () => {
            return runFilter().then(() => {
                expectCacheStoreWithTTL(500);
            });
        });

        it('stores with normal ttl, ignoring age=-100', () => {
            fakeResponse.headers.age = '-100';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

        it('stores with normal ttl, ignoring age=badstring', () => {
            fakeResponse.headers.age = 'badstring';

            return runFilter().then(() => {
                expectCacheStoreWithTTL(600);
            });
        });

    }); // end age header

});
