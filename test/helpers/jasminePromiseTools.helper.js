'use strict';

const jasminePromiseTools = require('jasmine-promise-tools');

global.expectToReject = jasminePromiseTools.expectToReject;
