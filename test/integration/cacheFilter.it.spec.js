'use strict';

const jasmineHttpSpy = require('jasmine-http-server-spy');
const httplease = require('httplease');

const createCacheFilter = require('../../index').createCacheFilter;

const BODY_CONTENT = {some: 'body'};

describe('integration/cacheFilter', () => {

    let httpSpy;
    let httpClient;

    beforeAll(() => {
        httpSpy = jasmineHttpSpy.createSpyObj('mockServer', [
            {
                handlerName: 'getHandler',
                method: 'get',
                url: '/get'
            }
        ]);

        return httpSpy.server.start(8082);
    });

    afterAll(() => {
        return httpSpy.server.stop();
    });

    beforeEach(() => {
        httpClient = httplease.builder()
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withTimeout(5000)
            .withBaseUrl('http://localhost:8082')
            .withFilter(createCacheFilter())
            .withPath('/get');

        jasmine.clock().install();
        jasmine.clock().mockDate(new Date('2016-01-01 10:00:00 UTC'));
    });

    afterEach(() => {
        httpSpy.getHandler.calls.reset();

        jasmine.clock().uninstall();
    });

    function setupCacheableResource(headers) {
        const extraHeaders = {
            'Date': '2016-01-01 10:00:00 UTC',
            'Cache-Control': 'max-age=600'
        };

        httpSpy.getHandler.and.returnValue({
            body: BODY_CONTENT,
            headers: Object.assign({}, extraHeaders, headers)
        });
    }

    it('caches responses with max-age', () => {
        setupCacheableResource();

        return httpClient.send()
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                jasmine.clock().mockDate(new Date('2016-01-01 10:10:00 UTC'));

                return httpClient.send();
            })
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                expect(httpSpy.getHandler.calls.count()).toBe(1);
            });
    });

    it('caches responses with expires header', () => {
        setupCacheableResource({
            'Cache-Control': undefined,
            'Expires': '2016-01-02 12:00:00 UTC'
        });


        return httpClient.send()
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                jasmine.clock().mockDate(new Date('2016-01-02 11:59:59 UTC'));

                return httpClient.send();
            })
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                expect(httpSpy.getHandler.calls.count()).toBe(1);
            });
    });

    it('expires cached resources', () => {
        setupCacheableResource();

        return httpClient.send()
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                jasmine.clock().mockDate(new Date('2016-01-01 10:10:01 UTC'));

                return httpClient.send();
            })
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                expect(httpSpy.getHandler.calls.count()).toBe(2);
            });
    });

    it('respects no-cache even with max-age', () => {
        setupCacheableResource({
            'Cache-Control': 'max-age=600,no-cache'
        });

        return httpClient.send()
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                return httpClient.send();
            })
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                expect(httpSpy.getHandler.calls.count()).toBe(2);
            });
    });

    it('respects no-cache even with expires', () => {
        setupCacheableResource({
            'Cache-Control': 'no-cache',
            'Expires': '2099-01-01 00:00:00 UTC'
        });

        return httpClient.send()
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                return httpClient.send();
            })
            .then((response) => {
                expect(response.body).toEqual(BODY_CONTENT);

                expect(httpSpy.getHandler.calls.count()).toBe(2);
            });
    });

});
